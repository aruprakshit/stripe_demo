$(function() {
  function checkForm() {
    var empty = false;

    $('.job-form .input').each(function() {
      if (this.type === 'checkbox') {
        if (!this.checked) empty = true;
      } else if ($(this).val() === '') {
        empty = true;
      }
    });

    if (empty) {
      // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
      $('.job-form .job-submit-btn').attr('disabled', 'disabled');
    } else {
      // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
      $('.job-form .job-submit-btn').removeAttr('disabled');
    }
  }

  $(".job-form .input").on("keyup change", checkForm);
  $('.job-form .job-submit-btn').attr('disabled', 'disabled');
  checkForm();
});

var StripeButton = function (key, data) {

  function submitForm(token) {
    var $form = $('.job-form');

    $('<input/>', {
      name: 'job[stripe_token]',
      val: token.id,
      type: 'hidden'
    }).appendTo($form);

    $form.submit()
  }

  var handler = StripeCheckout.configure({
    key: key,
    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
    locale: 'auto',
    token: submitForm
  });

  $('form .job-submit-btn').on('click', function(e) {
    // Open Checkout with further options:
    handler.open({
      name: 'Stripe.com',
      description: '2 widgets',
      zipCode: true,
      amount: data.amount,
      email: data.email
    });

    e.preventDefault();
  });

  $(window).on('popstate', function() {
    handler.close();
  });
};