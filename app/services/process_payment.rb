class ProcessPayment
  attr_reader :error

  def initialize token, amount, email
    @token  = token
    @amount = amount
    @email  = email
    @error  = nil
  end

  def run
    customer = Stripe::Customer.create(
      :email   => @email,
      :source  => @token
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @amount,
      :description => 'Rails Stripe customer',
      :currency    => 'usd'
    )
  rescue Stripe::CardError => e
    @error = e.message
  end
end