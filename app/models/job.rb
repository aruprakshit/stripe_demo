class Job < ApplicationRecord
  validates :title, :location, :description, presence: true
end
